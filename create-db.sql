create table if not exists users (
    id integer primary key autoincrement,
    username text not null,
    password text not null,
    cookies integer not null
);

create table if not exists buildings (
    id integer primary key autoincrement,
    name text not null,
    description text not null,
    cps integer not null
);

create table if not exists upgrades (
    id integer primary key autoincrement,
    name text not null,
    description text not null
);

create table if not exists user_buildings (
    id_user integer not null,
    id_building integer not null,
    count integer not null,

    foreign key (id_user) references users(id),
    foreign key (id_building) references buildings(id),

    primary key (id_user, id_building)
);

create table if not exists user_upgrades (
    id_user integer not null,
    id_upgrade integer not null,

    foreign key (id_user) references users(id),
    foreign key (id_upgrade) references upgrades(id),

    primary key (id_user, id_upgrade)
);