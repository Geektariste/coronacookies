<?php

namespace Cookies\utils;

use Cookies\model\User;

class Session
{
    private const USER = 'user';

    private bool $has_error;
    private string $error;

    public function __construct()
    {
        session_start();

        $this->has_error(false);
        $this->error('');
    }

    public function destroy() : void
    {
        session_destroy();
    }

    /**
     * Getter for logged in user.
     * 
     * @return User
     */
    public function user() : ?User
    {        
        if(!isset($_SESSION[self::USER]))
        {
            return null;
        }

        return $_SESSION[self::USER];
    }

    /**
     * Setter for logged in user
     * 
     * @return void
     */
    public function set_user(?User $user) : void
    {
        $_SESSION[self::USER] = $user;
    }

    /**
     * Hybrid getter / setter for the presence of an error.
     * 
     * @param bool $has_error
     *      If a value is provided, this acts like a setter, else, as a getter
     * 
     * @return bool
     */
    public function has_error(bool $has_error = null) : bool
    {
        if(isset($has_error))
        {
            $this->has_error = $has_error;
        }

        return $this->has_error;
    }

    /**
     * Hybrid getter / setter for an eventual error message.
     * 
     * @param string $error
     *      If a value is provided, this acts like a setter, else, as a getter
     * 
     * @return string
     */
    public function error(string $error = null) : ?string
    {
        if(isset($error))
        {
            $this->error = $error;
        }

        return $this->error;
    }
}