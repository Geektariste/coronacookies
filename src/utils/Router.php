<?php

namespace Cookies\utils;

use Cookies\controller\BaseController;
use Cookies\controller\Error404Controller;
use Cookies\controller\Error500Controller;
use Cookies\controller\GameController;
use Cookies\controller\HomeController;
use Cookies\controller\LogOutController;
use Cookies\controller\SignInController;
use Cookies\controller\SignUpController;


use Exception;
use PDOException;

class Router
{
    private const SIGN_IN = "signin";
    private const SIGN_UP = "signup";
    private const LOG_OUT = "logout";
    private const GAME    = "game";

    private BaseController $controller;
    private Session $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    public function run()
    {
        try{
            // route = section pour toi
            if(isset($_GET['route']))
            {
                switch($_GET['route']){
                    case self::SIGN_IN:
                        $this->controller = new SignInController();
                        break;

                    case self::SIGN_UP:
                        $this->controller = new SignUpController();
                        break;

                    case self::LOG_OUT:
                        $this->controller = new LogOutController();
                        break;

                    case self::GAME:
                        $this->controller = new GameController();
                        break;

                    default:
                        $this->controller = new Error404Controller();
                        break;
                }
            }
            else{
                $this->controller = new HomeController();
            }
        }
        catch (Exception $e)
        {
            $this->controller = new Error500Controller();
        }

        $this->controller -> setSession($this->session);
        $this->controller -> render();
    }

    public static function redirect(string $route = null) : void
    {
        $url = '/';
        if(isset($route)){
            $url .= '?route=' . $route;
        }

        header('Location:' . $url);
    }
}