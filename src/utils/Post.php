<?php

namespace Cookies\utils;


class Post
{
    private const USERNAME = 'username';
    private const PASSWORD = 'password';

    private function get_parameter(string $param_name) : ?string
    {
        if(isset($_POST[$param_name]))
        {
            return $_POST[$param_name];
        }

        return null;
    }

    public function username() : ?string
    {
        return $this -> get_parameter(self::USERNAME);
    }

    public function password() : ?string
    {
        return $this -> get_parameter(self::PASSWORD);
    }
}