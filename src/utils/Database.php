<?php

namespace Cookies\utils;

use Exception;
use PDO;

use Cookies\model\User;

class Database
{
    //Nos constantes
    const DB_PATH = '../cookies.db';
    const DB_HOST = 'sqlite:';

    private ?PDO $connection;

    public function __construct()
    {
        $this->connection = null;
    }

    public function __destruct()
    {
        $this->connection = null;
    }

    /**
     * Tries to connect to the database. It should be used only once, else, it will fail.
     */
    public function connect() : void
    {
        //Tentative de connexion à la base de données
        try{
            // If we're already connected, raise an error
            if(isset($this->connection)){
                throw new Exception("Database already opened");
            }

            $this->connection = new PDO(self::DB_HOST . self::DB_PATH);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //On renvoie la connexion
            return;
        }
        //On lève une erreur si la connexion échoue
        catch(Exception $errorConnection)
        {
            die ('Failed to access database :'.$errorConnection->getMessage());
        }
    }

    /**
     * Fetches all users
     */
    public function fetchAllUsers()
    {
        /*
        // requetes
        $result = $request -> fetchAll();

        $users = array();
        foreach($result as $row)
        {
            // on ajoute un user à la liste
            $users[] = new User($row['id'], blabla);
        }

        return $users;
        */
    }

    /**
     * Fetches a user based on his id
     * 
     * @param int id
     *      User id to fetch
     */
    public function fetchUser(int $id) : User
    {   
        $request = $this->connection -> prepare("
            SELECT id, username, cookies 
            FROM users 
            WHERE id = :id
        ");

        $request -> execute(array(
            ':id' => $id
        ));

        $result = $request -> fetch();

        return new User($result['id'], $result['username'], $result['cookies']);
    }

    /**
     * Tries to connect a user.
     * 
     * @param string $username
     *      Username
     * @param string $password
     *      Non-hashed password
     * 
     * @return ?User
     *      Returns 
     *      - null if wrong username or password
     *      - the requested user if connection is a success.
     */
    public function trySignIn(string $usernmame, string $password) : ?User
    {
        $request = $this->connection->prepare("
            SELECT id, username, cookies 
            FROM users 
            WHERE username = :username and password = :password
        ");

        $request -> execute(array(
            ':username' => $usernmame,
            ':password' => self::hash_password($password),
        ));

        $result = $request -> fetch();

        $user = null;
        if($result){
            $user = new User($result['id'], $result['username'], $result['cookies']);
        }
        
        return $user;
    }

    /**
     * Tries to sign up a user.
     * 
     * @param string $username
     *      Username
     * @param string $password
     *      Non-hashed password
     * 
     * @return ?User
     *      Returns 
     *      - null if wrong format or already used password. Format should be enforced by the webpage.
     *      - the created user if sign up is a success.
     */
    public function trySignUp(string $username, string $password) : ?User
    {
        /*
         * First, we check if the user already exists
         */
        $request = $this->connection -> prepare("
            SELECT *
            FROM users
            WHERE username=:username;
        ");

        $request -> execute([':username' => $username]);
        $result = $request -> fetch();

        /*
         * If a user already exists, we return null (the user couldn't be registered)
         */ 
        if($result){
            return null;
        }

        /*
         * Else, we register the user.
         */ 
        $request = $this->connection -> prepare("
            INSERT INTO users (username, password, cookies)
            VALUES (:username, :password, 0)
        ");

        $request -> execute(array(
            ':username' => $username,
            ':password' => self::hash_password($password),
        ));

        $id = $this->connection -> lastInsertId();
        
        return $this -> fetchUser($id);
    }


    public function saveUser(User $user)
    {
        // update user where ...
    }

    /*
     * Static functions
     */ 

    /**
     * Hashes a password in sha256.
     * 
     * @param string $password
     *      The password to hash
     * 
     * @return string
     *      The hashed password
     */
    private static function hash_password(string $password){
        return hash("sha256", $password);
    }
}
