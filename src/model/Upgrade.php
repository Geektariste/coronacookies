<?php

namespace Cookies\model;


class Building
{
    private string $name;
    private string $description;
    private int $cost;

    public function __construct(string $name, string $description, int $cost){
        $this->name = $name;
        $this->description = $description;
        $this->cost = $cost;
    }
}
