<?php

namespace Cookies\model;


class Building
{
    private string $name;
    private string $description;
    private int $cost;
    private int $cps;

    public function __construct(string $name, string $description, int $cost, int $cps){
        $this->name = $name;
        $this->description = $description;
        $this->cost = $cost;
        $this->cps = $cps;
    }
}
