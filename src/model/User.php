<?php

namespace Cookies\model;


class User
{
    private int $id;
    private int $cookies;

    private string $username;

    private array $buildings;
    private array $upgrades;

    public function __construct(int $id, string $username, int $cookies)
    {
        $this->id = $id;
        $this->username = $username;
        $this->cookies = $cookies;
    }
    
    public function username(string $username = null)
    {
        if(isset($username))
        {
            $this->username = $username;
        }

        return $this->username;
    }
}