<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;


class Error404Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->title = 'Perdu ?';
        $this->content_filename = 'error/404';
    }

    public function handle() : void
    {
        http_response_code(404);
    }
}