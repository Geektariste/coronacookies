<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;


class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->title = 'Accueil';
        $this->content_filename = 'home';
    }

    public function handle() : void
    {
        // Nothing to handle there
    }
}