<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;
use Cookies\utils\Router;

class GameController extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->title = 'Jeu';
        $this->content_filename = 'game';
    }

    public function handle() : void
    {
        if(is_null($this->session->user()))
        {
            Router::redirect('signin');
        }
    }
}