<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;
use Cookies\utils\Router;

class SignUpController extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->title = 'Inscription';
        $this->content_filename = 'signup';
    }

    public function handle() : void
    {
        /*
         * If we're registering, username and password should be sent as POST
         */
        $username = $this->post -> username();
        $password = $this->post -> password();

        if(isset($username) and isset($password))
        {
            $this->database -> connect();
            $user = $this->database -> trySignUp($username, $password);

            if(is_null($user)){
                $this->session -> has_error(true);
            }
            else
            {
                $this->session -> set_user($user);
                Router::redirect('game');
            }
        }
    }
}