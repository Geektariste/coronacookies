<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;
use Cookies\utils\Router;

class SignInController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        
        $this->title = 'Connexion';
        $this->content_filename = 'signin';    
    }

    public function handle() : void
    {
        /*
         * If we're connecting, username and password should be sent as POST
         */
        $username = $this->post -> username();
        $password = $this->post -> password();

        if(isset($username) and isset($password))
        {
            $this->database -> connect();
            $user = $this->database -> trySignIn($username, $password);

            if(is_null($user))
            {
                $this->session -> has_error(true);
            } 
            else
            {
                $this->session -> set_user($user);
                Router::redirect('game');
            }
        }
    }
}