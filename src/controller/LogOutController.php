<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;
use Cookies\utils\Router;

class LogOutController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        
        $this->has_display = false;
    }

    public function handle() : void
    {
        $this->session -> destroy();
        Router::redirect();
    }
}