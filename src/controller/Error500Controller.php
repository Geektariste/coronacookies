<?php

namespace Cookies\controller;

use Cookies\controller\BaseController;


class Error500Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->title = 'Erreur';
        $this->content_filename = 'error/500';
    }

    public function handle() : void
    {
        http_response_code(500);
    }
}