<?php

namespace Cookies\controller;

use Cookies\utils\Database;
use Cookies\utils\Post;
use Cookies\utils\Session;

abstract class BaseController
{
    private const TEMPLATE_NAME = 'template';

    protected Database $database;
    protected Session $session;
    protected Post $post;

    protected string $title;
    protected string $content_filename;
    protected bool $has_display;

    public function __construct()
    {
        $this->database = new Database();
        $this->post = new Post();

        $this->has_display = true;
    }

    public function render() : void
    {
        $this->handle();

        if($this->has_display)
        {
            ob_start();
            $this->load_view($this->content_filename);
            $this->content = ob_get_clean();

            $this->load_view(self::TEMPLATE_NAME);
        }
    }

    public function setSession(Session $session) : void
    {
        $this->session = $session;
    }

    abstract protected function handle() : void;

    private function load_view($filename) : void
    {
        require '../src/view/' . $filename . '.php';
    }
}