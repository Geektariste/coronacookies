<div class="centered">
    <h1>Connexion</h1>

    <!-- In case of error, display it -->
    <?php if($this->session->has_error()): ?>
        <div class="alert alert-danger">
            <h4 class="alert-heading">Raté.</h4>
            Vérifie tes identifiants ! Si tu n'est pas incrit, <a class="alert-link" href="/?route=signup">c'est ici</a>.
        </div>
    <?php endif; ?>

    <form novalidate action="/?route=signin" method="POST">
        <div  id="error" class="error alert alert-warning">dummy</div>

        <div class="form-floating mb-3"> 
            <input name="username" id="username" class="form-control" type="text" pattern="[a-zA-Z][a-zA-Z0-9]{4,}" required>
            <label for="username">Pseudo</label>
        </div>

        <div class="form-floating mb-3">
            <input name="password" id="password" class="form-control" type="password" pattern="[a-zA-Z0-9]{8,}" required>
            <label for="password">Mot de passe</label>
        </div>

        <button type="submit" class="btn btn-primary">C'est parti pour les cookies !</button>                
    </form>

    <script src="script/validate.js"></script>
</div>
