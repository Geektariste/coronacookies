<div class="container-fluid">
    <div class="row game-container bg-secondary">
        <div class="col-2 bg-dark text-white">
            <h2 class="text-center">Infos</h2>
        </div>

        <div class="col-8 position-relative">
            <div class="position-absolute top-50 start-50 translate-middle">
                <img id="cookie" src="image/big_cookie.png" onClick='produce()'/>
                <h2><span id="bank" class="badge rounded-pill bg-dark position-relative start-50 translate-middle"></span></h2>
            </div>

            <a class="btn btn-primary position-absolute bottom-0 start-50 translate-middle" role="button">Sauvegarde</a>
        </div>

        <div class="col-2 bg-dark text-white">
            <div class="row">
                <h2 class="text-center">Upgrades</h2>
                <div id="upgrades"></div>
            </div>

            <div class="row">
                <h2 class="text-center">Buildings</h2>
                <div id="buildings"></div>
            </div>
        </div>
    </div>
</div>

<script src="script/game.js"></script>