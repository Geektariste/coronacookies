<div class="centered">
    <h1>Coronacookies</h1>

    <?php if(is_null($this->session->user())): ?>
        <p>Coronacookies est un jeu incroyable de production de cookies covidés.</p>
        <a href="/?route=signin" class="btn btn-primary">Connexion</a>
        <a href="/?route=signup" class="btn btn-warning">Rejoins-nous !</a>

    <?php else: ?>
        <p>Ah mais tu es déjà là !</p>
        <a href="/?route=game" class="btn btn-primary">Viens donc jouer</a>
        
    <?php endif; ?>
</div>