<!DOCTYPE html>
<html lang='fr-FR'>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?= $this->title ?> - Coronacookies</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" href="css/coronacookies.css" media="screen" type="text/css" />
    </head>

    <body>
        <div class="bg-dark text-white">
            <div class="container">
                <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
                    <a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
                    </a>

                    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
                        <li><a href="/" class="nav-link px-2 text-secondary">Accueil</a></li>
                        <li><a href="/?route=game" class="nav-link px-2 text-white">Jouer</a></li>
                        <li><a href="#" class="nav-link px-2 text-white">FAQ</a></li>
                        <li><a href="#" class="nav-link px-2 text-white">A propos</a></li>
                    </ul>

                    <div class="col-md-3 text-end">
                        <?php if(is_null($this->session->user())): ?>
                            <a href="/?route=signin" class="btn btn-outline-light me-2">Connexion</a>
                            <a href="/?route=signup" class="btn btn-warning">Inscription</a>
                        <?php else: ?>
                            <button type="button" class="btn btn-light"><b><?= $this->session->user()->username() ?></b></button>
                            <a href="/?route=logout" class="btn btn-warning" role="button">Déconnexion</a>
                        <?php endif; ?>
                    </div>
                </header>
            </div>
        </div>

        <?= $this->content ?>

        <footer>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </body>
</html>