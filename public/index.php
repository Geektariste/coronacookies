<?php

require "../vendor/autoload.php";

use Cookies\utils\Router;

$router = new Router();
$router->run();