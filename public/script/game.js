// ==================== Var globale ====================================
var bank=0; // total des stocks
var cps=0; // prod par seconde
var cpc=1000; // prod par clic

// ==================== DOM elements ===================================
var elem_info_cps = document.getElementById('info-cps');
var elem_info_cpc = document.getElementById('info-cpc');

var elem_bank = document.getElementById('bank');


// ==================== UpGrades =======================================
var prixUG=[10,100,500]; // prix des upgrades
var UGDone=[false,false,false]; // amelioration faite ou non
var nomUG=["Multiplier par 2 les clics","Multiplier par 2 les clics","Multiplier par 5 les clics"];
var effetUG=["clic=clic*2;","clic=clic*2;","clic=clic*5"];

// ==================== Buildings ======================================
var prixB=[200,1000,15000]; // prix buildings
var nbB=[0,0,0]; // nb de chaque building
var nomB=["Mamie","Patissier","Usine"];
var prodB=[1,5,20];

// ================== Prod per sec =====================================
var interval = setInterval('bank+=cps;displayBank();', 1000);

// =====================================================================
updateInfos();

// ================== Fonctions ========================================

// Bouton prod
function produce(){
	bank+=cpc;
	updateInfos();
}

// met à jour la prod
function updateInfos(){
	elem_bank.innerHTML = 'Banque : '+magnifizer(bank);

	elem_info_cps.innerHTML = 'CPS : '+magnifizer(cps);
	elem_info_cpc.innerHTML = 'CPC : '+magnifizer(cpc);
}

// teste si l'on doit activer ou desactiver un bouton
function test(){
	for(i=0; i<prixUG.length; i++){ // pour chaque upgrades
		if(!UGDone[i] && bank>prixUG[i]-1){ // si l'amelio n'a pas été faite, et qu'on a assez
			document.getElementById('buyUG'+i).disabled=false;
		}
		else if(!UGDone[i] && bank<prixUG[i]){
			document.getElementById('buyUG'+i).disabled=true;
		}
	}
	for(i=0;i<prixB.length;i++){
		if(bank>prixB[i]-1){
			document.getElementById('buyB'+i).disabled=false;
		}
		else{
			document.getElementById('buyB'+i).disabled=true;
		}
	}
	for(i=0;i<prixB.length;i++){
		if(nbB[i]>0){
			document.getElementById('saleB'+i).disabled=false;
		}
		else{
			document.getElementById('saleB'+i).disabled=true;
		}
	}
}

// fonction faisant l'opération a // 1000 (partie entiere de a/1000)
function divMille(a){
	var res = 0;
	while(a-1000>=0){
		a-=1000;
		res++;
	}
	return res;
}

// met un nombre à - de 3 chiffres du 3 chiffres (pour le magnifizer)
// ex : 2 => 002
function toThreeChar(num){
	var res = num.toString();
	switch(res.length){
		case 3:
			break;
		case 2:
			res='0'+res;
			break;
		case 1:
			res='00'+res;
			break;
		default:
			break;
	}
	return res;
}

// Met les virgules dans l'affichage des chiffres
function magnifizer(num){
	if(divMille(num)==0)
		return num+'';
	else{
		var reste = num%1000;
		var div = divMille(num);
		var res = toThreeChar(reste);
		while(div!=0){
			reste = div%1000;
			div=divMille(div);
			if(div==0 && reste!=0)
				res = reste+','+res;
			else if(div!=0)
				res = toThreeChar(reste)+','+res;
			}
		}
	return res;
}

// ================= Buildings =========================================

// actions réalisées lor d' lachat d'un building
function buildingBuy(num){
	bank-=prixB[num];
	displayBank();
	cps+=prodB[num];
	displayCPS();
	nbB[num]++;
	test();
	document.getElementById('nb'+num).innerHTML = nbB[num];
}

function buildingSale(num){
	bank+=prixB[num]/2;
	displayBank();
	cps-=prodB[num];
	displayCPS();
	nbB[num]--;
	test();
	document.getElementById('nb'+num).innerHTML = nbB[num];
}
// ================ Upgrade ============================================

// actions réalisées lors de l'achat d'un upgrade	
function upgrade(num){
	bank-=prixUG[num];
	displayBank();
	UGDone[num]=true;
	document.getElementById("done"+num).innerHTML="<img src='img/valid.png' width='20px' height='20px'>";
	test();
}