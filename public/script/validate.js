var form = document.getElementsByTagName('form')[0];
var username = document.getElementById('username');
var password = document.getElementById('password');
var error = document.getElementById('error');

function setError(errorMessage){
    error.innerHTML = errorMessage;
    error.style.visibility = 'visible';
}

function removeError(){
    error.style.visibility = 'hidden';
}

username.addEventListener("input", function(event){
    username.checkValidity();

    if(!username.validity.valid){
        setError("Pseudo de mini 5 lettres ou chiffres, et on commence par une lettre stp.");
    }
    else{
        removeError();
    }

    username.parentElement.classList.add('was-validated');
}, false);

password.addEventListener("input", function(event){
    password.checkValidity();

    if(!password.validity.valid){
        setError("Mot de passe de mini 8 lettres ou chiffres.");
    }
    else{
        removeError();
    }

    password.parentElement.classList.add('was-validated');
}, false);

form.addEventListener("submit", function (event) {
    // When user tries to submit, we check if he can
    if (!(username.validity.valid && password.validity.valid)) {
      // If he can't, we stop the submission
      event.preventDefault();
    }
}, false);